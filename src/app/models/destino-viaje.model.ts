import { v4 as uuid } from 'uuid';

export class DestinoViaje{
    selected: boolean;
    servicios: string[];
    id = uuid();
    
    constructor(public nombre: string, public imageUrl:string, public votes: number = 0){
        this.servicios = ['pileta', 'desayuno'];
    }

    isSelected(): boolean{
        return this.selected;
    }

    setSelected(select: boolean) {
        this.selected = select;
    }

    toString(): string{
        return `${this.nombre} - ${this.imageUrl}`; 
    }

    voteUp(){
        this.votes++;
    }

    voteDown(){
        this.votes--;
    }

    reset(){
        this.votes = 0;
    }
}