import { Component, Inject, OnInit, InjectionToken, Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { DestinosApiClient } from 'src/app/models/destinos-api-client.model';
import { DestinoViaje } from '../../models/destino-viaje.model';

// class DestinosApiClientViejo{
//   getById(id: String): DestinoViaje{
//     console.log('llamado por la clase vieja!');
//     return null;
//   }
// }

// interface AppConfig{
//   apiEndPoint: String;
// }

// const APP_CONFIG_VALUE: AppConfig = {
//   apiEndPoint: 'mi_api.com'
// }

// const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

// @Injectable()
// class DestinosApiClientDecorated extends DestinosApiClient{
  
//   constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>){
//     super(store);
//   }

//   getById(id: String): DestinoViaje{
//     console.log('llamando por la clase decorada');
//     console.log('config: ' + this.config.apiEndPoint);
//     return super.getById(id);
//   }
// }

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  styles: [`
    mgl-map {
      height: 100vh;
      width: 100vw;
    }
  `],
  providers: [
    // { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    // { provide: DestinosApiClient, useClass: DestinosApiClientDecorated },
    // { provide: DestinosApiClientViejo, useExisting: DestinosApiClient }
    DestinosApiClient
  ]
})

export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;

  style = {
    sources: {
      world: {
        type: "geojson",
        data: "https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json"
      }
    },
    version: 8,
    layers: [{
      "id": "countries",
      "type": "fill",
      "source": "world",
      "layout": {},
      "paint": {
        'fill-color': '#6F788A'
      }
    }]
  }
  
  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClient) { }

  ngOnInit(): void {
     let id = this.route.snapshot.paramMap.get('id');
     this.destino = this.destinosApiClient.getById(id);
  }

}
