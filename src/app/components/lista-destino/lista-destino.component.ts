import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css'],
  providers: [
    DestinosApiClient
  ]
})
export class ListaDestinoComponent implements OnInit { 
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[]; 
  all;
  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.items).subscribe(items => this.all = items);
    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        
        if(data != null){
          this.updates.push('Se ha elegido a ' + data.nombre);
        }
      });
  }

  ngOnInit(): void {
  }

  agregado(destino: DestinoViaje){
    this.destinosApiClient.add(destino);
    this.onItemAdded.emit(destino);
  }

  elegido(destino: DestinoViaje){
    this.destinosApiClient.elegir(destino);
  }

}
